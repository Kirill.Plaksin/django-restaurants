# django restaurants

How to install?

Just do:
```
docker-compose up --build
```

How to enter in backend container?
```
docker exec -it backend bash
```

How to run test in container?
```
./manage.py test restaurants.tests
```
Django Admin: http://localhost:8000/admin/

Frontend: http://localhost:8080/

Includes: 
```
Django
Django REST framework
Vue CLI 3
```

About
```
This is a simple web application.
Serves for searching and rating restaurants
```

Important note
```
Sometimes at the first build of the project
postgres does not have time to get up and there is no connection to the database
on this case just do CTRL+C and do docker-compose up
```

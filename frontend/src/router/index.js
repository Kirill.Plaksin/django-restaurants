import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Single from "../views/Single";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/restaurant-detail/:id',
        name: 'Single',
        component: Single,
        props: true
    },
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router

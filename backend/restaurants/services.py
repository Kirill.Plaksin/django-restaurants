from django.db.models.aggregates import Sum, Count
from django.db.models.expressions import F
from django_filters import rest_framework as filters

from .models import Restaurant


class CharFilterInFilter(filters.BaseInFilter, filters.CharFilter):
    pass


class RestaurantFilterSet(filters.FilterSet):
    name = CharFilterInFilter(field_name='name', lookup_expr='in')
    stars = filters.NumberFilter(method='_filter_middle_star')

    class Meta:
        model = Restaurant
        fields = ['name', 'stars']

    def _filter_middle_star(self, queryset, name, value):
        return (
            Restaurant.objects
            .annotate(middle_star=Sum(F('ratings__star')) / Count(F('ratings')))
            .filter(middle_star=value)
        )

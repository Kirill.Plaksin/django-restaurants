from django.urls import path
from . import views

urlpatterns = [
    path("", views.RestaurantView.as_view({'get': 'list'}), name='home'),
    path("restaurant-detail/<int:pk>", views.RestaurantView.as_view({'get': 'retrieve'}), name='restaurant-detail'),
    path("ratings/", views.RatingCreateViewSet.as_view({'post': 'create'})),

]

from django.db import models


class Restaurant(models.Model):
    """Restaurant"""
    name = models.CharField("Name of restaurant", max_length=150)
    photo = models.ImageField("Image", upload_to="images/")

    def __str__(self):
        return f'{self.name}'


class Address(models.Model):
    street = models.CharField('street', max_length=150)
    zip_code = models.CharField('zip-code', max_length=7)
    location = models.CharField('location', max_length=50)
    country = models.CharField('country', max_length=50)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, verbose_name="restaurant")


class Rating(models.Model):
    """Rating"""
    email = models.EmailField()
    text = models.TextField("Message", max_length=5000)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.CASCADE, verbose_name="restaurant", related_name="ratings")
    star = models.SmallIntegerField("value", default=0)

    def __str__(self):
        return f"{self.star} - {self.restaurant}"

    class Meta:
        verbose_name = "Rating"
        verbose_name_plural = "Ratings"

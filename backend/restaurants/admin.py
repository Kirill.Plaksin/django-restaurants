from django import forms
from django.contrib import admin

from .models import (
    Address,
    Restaurant,
    Rating,
)


class AddressInline(admin.TabularInline):
    model = Address
    fields = ('street', 'zip_code', 'location', 'country')


class RestaurantAdminForm(forms.ModelForm):
    class Meta:
        model = Restaurant
        fields = '__all__'


@admin.register(Restaurant)
class RestaurantAdmin(admin.ModelAdmin):
    form = RestaurantAdminForm
    inlines = [AddressInline, ]


@admin.register(Rating)
class UserRatingAdmin(admin.ModelAdmin):
    list_display = ("star", "restaurant", "email", 'text')

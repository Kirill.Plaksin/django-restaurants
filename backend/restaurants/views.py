from django.db.models.aggregates import (
    Sum,
    Count,
)
from django.db.models.expressions import F

from rest_framework.viewsets import (
    ReadOnlyModelViewSet,
    ModelViewSet,
)
from rest_framework.permissions import AllowAny
from django_filters.rest_framework import DjangoFilterBackend

from .models import Restaurant, Rating
from .serializers import (
    BaseRestaurantSerializer,
    CreateRatingSerializer,
    RestaurantDetailsSerializer,
)

from rest_framework.pagination import PageNumberPagination

from .services import RestaurantFilterSet


class StandardPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 10


class RestaurantView(ReadOnlyModelViewSet):
    permission_classes = [AllowAny]
    pagination_class = StandardPagination
    filter_backends = (DjangoFilterBackend,)
    filterset_class = RestaurantFilterSet

    def get_queryset(self):
        restaurant = Restaurant.objects.annotate(
            middle_star=Sum(F('ratings__star')) / Count(F('ratings'))
        )
        return restaurant

    def get_serializer_class(self):
        if self.action == 'list':
            return BaseRestaurantSerializer
        elif self.action == "retrieve":
            return RestaurantDetailsSerializer


class RatingCreateViewSet(ModelViewSet):
    permission_classes = [AllowAny]
    queryset = Rating.objects.none()
    serializer_class = CreateRatingSerializer

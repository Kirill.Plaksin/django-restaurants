# Generated by Django 3.0 on 2020-07-14 19:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Name of restaurant')),
                ('photo', models.ImageField(upload_to='images/', verbose_name='Image')),
            ],
        ),
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(max_length=254)),
                ('text', models.TextField(max_length=5000, verbose_name='Message')),
                ('star', models.SmallIntegerField(default=0, verbose_name='value')),
                ('restaurant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='ratings', to='restaurants.Restaurant', verbose_name='restaurant')),
            ],
            options={
                'verbose_name': 'Rating',
                'verbose_name_plural': 'Ratings',
            },
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('street', models.CharField(max_length=150, verbose_name='street')),
                ('zip_code', models.CharField(max_length=7, verbose_name='zip-code')),
                ('location', models.CharField(max_length=50, verbose_name='location')),
                ('country', models.CharField(max_length=50, verbose_name='country')),
                ('restaurant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='restaurants.Restaurant', verbose_name='restaurant')),
            ],
        ),
    ]

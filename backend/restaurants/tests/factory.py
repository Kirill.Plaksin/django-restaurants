import factory

from ..models import (
    Address,
    Restaurant,
    Rating,
)


class RestaurantFactory(factory.DjangoModelFactory):
    photo = factory.django.ImageField(name='image.jpg')
    name = 'Restaurant 1'

    class Meta:
        model = Restaurant


class AddressFactory(factory.DjangoModelFactory):
    street = 'Street 1'
    zip_code = '20-123'
    location = 'Warsaw'
    country = 'Poland'
    restaurant = factory.SubFactory(RestaurantFactory)

    class Meta:
        model = Address


class RatingFactory(factory.DjangoModelFactory):
    email = 'email@test.com'
    text = 'text'
    star = 5
    restaurant = factory.SubFactory(RestaurantFactory)

    class Meta:
        model = Rating

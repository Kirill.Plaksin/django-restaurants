from django.test import TestCase
from django.test import Client

from .factory import (
    RestaurantFactory,
    AddressFactory,
    RatingFactory,
)
from ..models import (
    Restaurant,
    Rating,
)


class RestaurantViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.restaurant = RestaurantFactory()
        cls.address = AddressFactory(restaurant=cls.restaurant)
        cls.rating = RatingFactory(restaurant=cls.restaurant)
        cls.client = Client()

    def test_list(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['count'], Restaurant.objects.count())
        self.assertEqual(data['results'][0]['id'], self.restaurant.pk)
        self.assertEqual(data['results'][0]['name'], self.restaurant.name)
        self.assertEqual(data['results'][0]['middle_star'], 5)

    def test_details(self):
        response = self.client.get(f'/restaurant-detail/{self.restaurant.pk}')
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['id'], self.restaurant.pk)
        self.assertEqual(data['name'], self.restaurant.name)
        self.assertEqual(data['middle_star'], 5)
        self.assertDictEqual(
            data['address'][0],
            {
                'street': self.address.street,
                'zip_code': self.address.zip_code,
                'location': self.address.location,
                'country': self.address.country
            }
        )
        self.assertDictEqual(
            data['ratings'][0],
            {
                'id': self.rating.pk,
                'star': self.rating.star,
                'email': self.rating.email,
                'text': self.rating.text
            }
        )

    def test_search_by_name_exist(self):
        response = self.client.get(f'/?name={self.restaurant.name}')
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['count'], Restaurant.objects.count())
        self.assertEqual(data['results'][0]['id'], self.restaurant.pk)
        self.assertEqual(data['results'][0]['name'], self.restaurant.name)
        self.assertEqual(data['results'][0]['middle_star'], 5)

    def test_search_by_name_not_exist(self):
        response = self.client.get('/?name=SomeName')
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['count'], 0)

    def test_search_by_middle_star(self):
        response = self.client.get('/?stars=5')
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['count'], Restaurant.objects.count())
        self.assertEqual(data['results'][0]['id'], self.restaurant.pk)
        self.assertEqual(data['results'][0]['name'], self.restaurant.name)
        self.assertEqual(data['results'][0]['middle_star'], 5)

    def test_search_by_middle_star_not_found(self):
        response = self.client.get('/?stars=2')
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertEqual(data['count'], 0)


class RatingCreateViewSetTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.restaurant = RestaurantFactory()
        cls.client = Client()

    def test_creating(self):
        data = {'star': 5, 'restaurant': self.restaurant.pk, 'text': 'message', 'email': 'testemail@test.com'}
        response = self.client.post('/ratings/', data=data)
        self.assertEqual(response.status_code, 201)
        rating = Rating.objects.get()
        self.assertEqual(data['star'], rating.star)
        self.assertEqual(data['restaurant'], self.restaurant.pk)
        self.assertEqual(data['text'], rating.text)
        self.assertEqual(data['email'], rating.email)

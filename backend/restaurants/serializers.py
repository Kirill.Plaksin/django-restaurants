from django.db import transaction
from rest_framework import serializers

from .models import (
    Address,
    Restaurant,
    Rating,
)


class CreateRatingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rating
        fields = ("star", "restaurant", 'email', 'text')

    @transaction.atomic
    def create(self, validated_data):
        rating, _ = Rating.objects.update_or_create(
            email=validated_data.get('email', None),
            restaurant=validated_data.get('restaurant', None),
            defaults={
                'star': validated_data['star'],
                'text': validated_data.get('text')
            }
        )
        return rating


class RatingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Rating
        fields = ('id', 'star', 'email', 'text')


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ['street', 'zip_code', 'location', 'country']


class BaseRestaurantSerializer(serializers.ModelSerializer):
    middle_star = serializers.IntegerField()

    class Meta:
        model = Restaurant
        fields = ('id', 'name', 'photo', 'middle_star')


class RestaurantDetailsSerializer(BaseRestaurantSerializer):
    address = AddressSerializer(source='address_set', many=True, read_only=True)
    ratings = RatingSerializer(many=True, read_only=True)

    class Meta(BaseRestaurantSerializer.Meta):
        model = Restaurant
        fields = BaseRestaurantSerializer.Meta.fields + ('address', 'ratings',)
